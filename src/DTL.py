import pandas as pd 
from math import log
import numpy as np 



#Importing the data from the csv file into a list of pandas data frame rows. 

filepath = "C:\\Users\\Ozan Gokdemir\\Desktop\\gokdemir-kwan-kane\\data\\titanic-data.csv"
dataset = pd.read_csv(filepath)



#Helper function for cleaning the data.
#Updating the Age attribute. Encoding the age values into categories based on the age interval. 
def encodeAgeAndFareValues(dataset):
        dataset.loc[(dataset['Age']) <= 12, ["Age"]] = 1 #children-1
        dataset.loc[(dataset['Age'] >12) & (dataset["Age"]<=20), ["Age"]] = 2 #teens2
        dataset.loc[(dataset['Age'] > 20) & (dataset["Age"]<= 40), ["Age"]] = 3 # young adults-3
        dataset.loc[(dataset['Age'] > 40) & (dataset["Age"]<= 60), ["Age"]] = 4 # mid age - 4
        dataset.loc[(dataset['Age'] > 60) & (dataset["Age"]<= 80) , ["Age"]] = 5 # old -5
        dataset.loc[dataset['Age'].isnull() , ["Age"]] = 0 # missing values.
        
        dataset.loc[(dataset['Fare']) <= 10, ["Fare"]] = 1 #cheap tickets.
        dataset.loc[(dataset['Fare'] >10) & (dataset["Fare"]<=30), ["Fare"]] = 2 #somewhat cheap tickets.
        dataset.loc[(dataset['Fare'] > 30) & (dataset["Fare"]<= 60), ["Fare"]] = 3 # decent tickets
        dataset.loc[(dataset['Fare'] > 60) & (dataset["Fare"]<= 100), ["Fare"]] = 4 # somewhat expensive tickets.
        dataset.loc[dataset['Fare'] > 100 , ["Fare"]] = 5 # most expensive tickets.
        dataset.loc[dataset['Fare'].isnull() , ["Fare"]] = 0 # missing values.
        
        
#This function call is for cleaning the data.
        
encodeAgeAndFareValues(dataset)

testlist = []
for idx, row in dataset.iterrows():
    testlist.append(row)
    
data = testlist




#global_features list stores all the features in the original dataset.
#currently used for testing purposes.
#some stuff will probably will be removed out of this list as the recursion moves forward.
global_features = []
for feature in dataset:
    global_features.append(feature)



#Implements the main DTL algorithm.
# The class should somehow be encoded in the datapoints
#  structure. A reasonable approach would be to use
#  the feature name "class" to refer to it consistently.

def DTL(datapoints, features, default_value):
    pass    



#Helper function to calculate the entropy of a dataset. 
#Our class attribute is named "Class" in the data file. 
def entropy(datapoints):
    num_datapoints = len(datapoints)
    
    #Stores each different value the class can take, and their count in the dataset.
    labels = {}
    #Go through each row in the dataset, tally the value of the class attribute. 
    for row in datapoints:
        label = row["Class"]
        if label not in labels.keys():
            labels[label] = 0
        else:
            labels[label] += 1

    
    #Start calculating the entropy.
    entropy = 0.0
    for key in labels:
        proportion = float(labels[key])/num_datapoints
        
        if(proportion == 0):
            proportion=0.0001
        entropy -= proportion * log(proportion,2)
        
    return entropy



#Returns a set of subsets of datapoints
# such that all points in a subset have
# the same value of the indicated feature.
    
'''
@Params: 
    datapoints: the dataset at hand. will change in each recursive iteration.
    feature: the feature for which all the datapoints in the subsets have the same value.

'''
def Make_Subsets(datapoints, feature):
    #maps the possible value to a list of datapoints which has that value.
    subsets = {}  
    for row in datapoints:
        
        if row[feature] in subsets.keys():
            subsets[row[feature]].append(row)
            
        else:
            subsets[row[feature]] = [row]
        
            
    return subsets

#a =Make_Subsets(data, "Sex")

#Returns the feature with the greatest information
# gain for the provided datapoints.
def Choose_Attribute(features, datapoints):
    features.remove("Class")
    
    #dictionary that maps the features to the information gain.
    info_gains = {}
    
    parent_entropy = entropy(datapoints)
    
    for test_feature in features:
        info_gain=parent_entropy
        #getting a dictionary that maps value --> set of data points.
        subsets = Make_Subsets(datapoints, test_feature)
        for value, subset in subsets.items():
            
            info_gain -= (len(subset)/len(dataset))*entropy(subset)
        info_gains[test_feature]=info_gain
        
    best_feature = max(info_gains, key=info_gains.get)
    print(best_feature)
    print(info_gains)
    
        

Choose_Attribute(global_features, data)



#Computes the mode of the class of datapoints.
#Only for the base case. When data has no features.
def Compute_Mode(datapoints):
    labels = {}
    for index, row in datapoints.iterrows(): 
        label = row["Class"]
        
        if label not in labels.keys():
            labels[label] = 0
        else:
            labels[label] += 1 
    
    return min(labels, key=labels.get)



#Computes the information entropy of datapoints w.r.t.
# the class.

'''
@params:
    entropy_before: entropy of the set that datapoints is the subset of.
    datapoints: the subset of the data(split) whose entropy will be compared to the entropy of the set before the split.
    
    Note: We need to pass the entropy-before-split into the splitted set in order to calculate the info gain.
'''
def Compute_Information(entropy_before, datapoints):
    return entropy_before - entropy(datapoints)





